# LIDO2RDF

RML transformation of LIDO/XML data to RDF.

To work with this transformation pipeline, clone the repository locally `git clone https://gitlab.com/nfdi4culture/ta5-knowledge-graph/lido2rdf`.

### Prerequisites

This transformation process requires [java](https://www.java.com/), which can be verified as installed by running `java -version`.

If it is not present, it can be installed on most Linux distributions with `sudo apt install default-jre`.

[RMLMapper-java](https://github.com/RMLio/rmlmapper-java) is the engine used in the follow examples to enact the transformation, following the mapping specified in `lido.rml`. RMLMapper can be downloaded [here](https://github.com/RMLio/rmlmapper-java/releases) and placed in the cloned repo. The following examples use version `6.1.3`.

### LIDO

[LIDO](https://cidoc.mini.icom.museum/working-groups/lido/lido-overview/about-lido/what-is-lido/) (Lightweight Information Describing Objects) is an XML Schema based on [CIDOC CRM](https://www.cidoc-crm.org/), intended for general use by cultural institutions. 

This transformation aims to convert incoming LIDO/XML files to [RDF](https://www.w3.org/TR/PR-rdf-syntax/Overview.html) to enable injection into a [triplestore](https://en.wikipedia.org/wiki/Triplestore), making the dataa accessible for harmonisation or [SPARQL](https://www.w3.org/TR/sparql11-query/) querying. The [Linked Art](https://linked.art/) project pursues similar aims, using a dedicated interpretation of CIDOC for the generation of JSON-LD data representations.

### Example 

A LIDO example file can be fetched with `curl -o lido.xml http://www.lido-schema.org/documents/examples/LIDO-v1.1-Example_FMobj20344012-Fontana_del_Moro.xml` run within the cloned repo directory. Other LIDO files can be inserted in place of the downloaded `lido.xml` file.

The transformation mapping can be run with `java -jar rmlmapper-6.1.3-r367-all.jar -m lido.rml -o lido.ttl -s turtle`, which results in the incoming `lido.xml` file rendered to `lido.ttl`. The `-s` flag in the command can be modified to specify alternate serialisations.

### Licence

This transformation process is released under `MIT Licence`.

